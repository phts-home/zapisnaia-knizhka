unit Unit3;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, shellapi;

type
  TForm3 = class(TForm)
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Button_close: TButton;
    Label8: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    procedure Label4Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Label9Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

uses Unit1;

{$R *.dfm}

procedure TForm3.Label4Click(Sender: TObject);
begin
ShellExecute(0,pchar(''),pchar('mailto:'+Label4.Caption+'?subject=PhoneBook'),pchar(''),pchar(''),1);
end;

procedure TForm3.Timer1Timer(Sender: TObject);
begin
label1.Font.Color:=random($ffffff);
end;

procedure TForm3.Label9Click(Sender: TObject);
begin
ShellExecute(0,pchar(''),pchar(Label9.Caption),pchar(''),pchar(''),1);
end;

procedure TForm3.FormShow(Sender: TObject);
begin
form3.Left:=form1.Left+200;
form3.Top:=form1.Top+180;
end;

end.
