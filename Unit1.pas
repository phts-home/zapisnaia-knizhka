unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls;

type
  TForm1 = class(TForm)
    ListBox1: TListBox;
    Edit_mob: TEdit;
    Edit_dom: TEdit;
    Edit_adr: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Memo1: TMemo;
    Image1: TImage;
    Panel1: TPanel;
    Image_closeButtonPushed: TImage;
    Image_closebuttonDefault: TImage;
    Label_count: TLabel;
    Panel2: TPanel;
    Image_helpButtonPushed: TImage;
    Image_helpButtonDefault: TImage;
    Panel3: TPanel;
    Panel4: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure Image_closeButtonPushedClick(Sender: TObject);
    procedure Image_closebuttonDefaultMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure Image_helpButtonDefaultMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure Image_helpButtonPushedClick(Sender: TObject);
    procedure Panel3Click(Sender: TObject);
    procedure Panel4Click(Sender: TObject);
    procedure Panel4MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Panel3MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure WMNCHitTest(var M: TWMNCHitTest); message wm_NCHitTest;
  end;

var
  Form1: TForm1;
  datafile,CurrentName:string;
  dat:textfile;

implementation

uses Unit2, Unit3;

{$R *.dfm}

procedure FindItem (ItemName:string;MiscIt:TMemo; var MobTel,DomTel,Adress:string );
var st,sr:string;
    i,n,l:word;
begin
assignfile(dat,datafile);
reset(dat);
while not eof(dat) do
  begin
  readln(dat,st);
  n:=1;                           //���. ������ ������� �����
  l:=1;                           //����� ����� � ������
  for i:=1 to length(st) do
    begin
    if st[i]='*'
      then
      begin
      sr:=copy(st,n,i-n);
      case l of
        1:  begin
            if sr<>itemname
              then break;
            end;
        2: MobTel:=sr;
        3: DomTel:=sr;
        4: Adress:=sr;
//        end
        else MiscIt.Lines.Add(sr);
        end;
      n:=i+1;
      inc(l);
      end;
    end;
  end;
closefile(dat);
end;

procedure AddItem (nameItem,mobItem,domItem,AdrItem:string; miscItem:Tstrings);
var i:word;
begin
assignfile(dat,datafile);
append(dat);
write(dat,nameItem+'*'+mobItem+'*'+domItem+'*'+AdrItem+'*');
while i<miscItem.Count do
  begin
  write(dat,miscitem[i]+'*');
  inc(i);
  end;

writeln(dat,'');
closefile(dat);
end;

procedure DelItem (ItemName:string);
var i:word;
    st,sr:string;
    temp:textfile;
begin
assignfile(dat,datafile);
assignfile(temp,extractfilepath(paramstr(0))+'temp.dat');
try reset(dat)
  except
    rewrite(dat);
    closefile(dat);
    reset(dat);
  end;
rewrite(temp);
while not eof(dat) do
  begin
  readln(dat,st);
  for i:=1 to length(st) do
    begin
    if st[i]='*'
      then
      begin
      sr:=copy(st,1,i-1);
      if sr<>ItemName//listbox1.Items[listbox1.ItemIndex]
        then writeln(temp,st);
      break;
      end;
    end;
  end;
closefile(dat);
closefile(temp);
reset(temp);
rewrite(dat);
while not eof(temp) do
  begin
  readln(temp,st);
  writeln(dat,st);
  end;
closefile(dat);
closefile(temp);
erase(temp);

end;

procedure TForm1.FormCreate(Sender: TObject);
var st,sr:string;
    i:word;
begin
datafile:=extractfilepath(paramstr(0))+'phones.dat';
assignfile(dat,datafile);
try reset(dat)
  except
    rewrite(dat);
    closefile(dat);
    reset(dat);
  end;
while not eof(dat) do
  begin
  readln(dat,st);
  for i:=1 to length(st) do
    begin
    if st[i]='*'
      then
      begin
      sr:=copy(st,1,i-1);
      listbox1.Items.Add(sr);
      break;
      end;
    end;
  end;
closefile(dat);
Label_count.Caption:=inttostr(listbox1.Items.count);
end;

procedure TForm1.ListBox1Click(Sender: TObject);
var Smob,Sdom,Sadr:string;
begin
memo1.Clear;
currentname:=listbox1.Items[listbox1.itemindex];
FindItem(listbox1.Items[listbox1.itemindex],memo1,Smob,Sdom,Sadr);
edit_mob.Text:=smob;
edit_dom.text:=sdom;
edit_adr.text:=sadr;
end;

procedure TForm1.SpeedButton1Click(Sender: TObject);
begin
form2.Edit1.Text:=currentname;
form2.showmodal;
if form2.ModalResult=mrOK
  then listbox1.Items.Add(form2.Edit1.text);
AddItem(form2.Edit1.text,edit_mob.text,edit_dom.text,edit_adr.Text,memo1.Lines);
Label_count.Caption:=inttostr(listbox1.Items.count);
end;

procedure TForm1.SpeedButton2Click(Sender: TObject);
var i:word;
    st,sr:string;
    temp:textfile;
begin
assignfile(dat,datafile);
assignfile(temp,extractfilepath(paramstr(0))+'temp.dat');
try reset(dat)
  except
    rewrite(dat);
    closefile(dat);
    reset(dat);
  end;
rewrite(temp);
while not eof(dat) do
  begin
  readln(dat,st);
  for i:=1 to length(st) do
    begin
    if st[i]='*'
      then
      begin
      sr:=copy(st,1,i-1);
      if sr<>listbox1.Items[listbox1.ItemIndex]
        then writeln(temp,st);
      break;
      end;
    end;
  end;
closefile(dat);
closefile(temp);
reset(temp);
rewrite(dat);
while not eof(temp) do
  begin
  readln(temp,st);
  writeln(dat,st);
  end;
closefile(dat);
closefile(temp);
erase(temp);

listbox1.Items.Delete(listbox1.ItemIndex);
Label_count.Caption:=inttostr(listbox1.Items.count);
end;

procedure TForm1.WMNCHitTest(var M: TWMNCHitTest);
begin
inherited;
if M.Result = htClient then M.Result := htCaption;
image_closebuttonPushed.hide;
image_closebuttonDefault.show;
image_helpbuttonPushed.hide;
image_helpbuttonDefault.show;
panel4.Color:=$00D7FEFF;
panel3.Color:=$00D7FEFF;
panel4.Font.Color:=clNavy;
panel3.Font.Color:=clMaroon;
end;


procedure TForm1.Image_closeButtonPushedClick(Sender: TObject);
begin
close;
end;

procedure TForm1.Image_closebuttonDefaultMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
image_closebuttonpushed.Show;
image_closebuttondefault.hide;
end;

procedure TForm1.Image_helpButtonDefaultMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
image_helpbuttonPushed.Show;
image_helpbuttonDefault.Hide
end;

procedure TForm1.Image_helpButtonPushedClick(Sender: TObject);
begin
form3.showmodal;
end;

procedure TForm1.Panel3Click(Sender: TObject);
begin
if listbox1.ItemIndex<>-1
  then
  begin
  DelItem(listbox1.Items[listbox1.ItemIndex]);
  end;
listbox1.Items.Delete(listbox1.ItemIndex);
Label_count.Caption:=inttostr(listbox1.Items.count);
end;

procedure TForm1.Panel4Click(Sender: TObject);
var i:word;
    t:boolean;
begin
t:=true;
form2.Edit1.Text:=currentname;
form2.showmodal;
if form2.ModalResult=mrOK
  then
  begin
  try
  for i:=0 to listbox1.items.count-1 do
    begin
    if listbox1.Items[i]=form2.Edit1.text
      then
      begin
      t:=false;
      break;
      end;
    end;
  except end;
  if t
    then
    begin
    listbox1.Items.Add(form2.Edit1.text);
    AddItem(form2.Edit1.text,edit_mob.text,edit_dom.text,edit_adr.Text,memo1.Lines);
    Label_count.Caption:=inttostr(listbox1.Items.count);
    end
    else
    begin
    if application.MessageBox('����� ����� ��� ����. ��������?','�������� ������',MB_ICONQUESTION+MB_OKCANCEL)=1//messagedlg('����� ����� ��� ����. ��������?',mtConfirmation,[mbYES,mbNO],0)=mrOK
      then
      begin
      DelItem(form2.Edit1.text);
      AddItem(form2.Edit1.text,edit_mob.text,edit_dom.text,edit_adr.Text,memo1.Lines);
      Label_count.Caption:=inttostr(listbox1.Items.count);
      end;
    end;
  end;

end;

procedure TForm1.Panel4MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
panel4.Color:=$00292929;
panel4.Font.Color:=$0000CCFF;
end;

procedure TForm1.Panel3MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
panel3.Color:=$00292929;
panel3.Font.Color:=$0000CCFF;
end;

end.
